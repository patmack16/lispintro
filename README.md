### This improved iteration runs the game via AI. It prints the game board automatically after each AI-move as well as the current move count. 

## To Run: Simply open the program in a lisp interpreter.  The start function is called automatically and all of the moves will be displayed. 

## a-i move: The base function takes in the original list and calls other functions to get the game moving.

## a-i index: called from a-i move.  Picks a random spot number from the board then calls toggle logic to see what spaces need to be toggled and ai-move-check on that returned list.

##  ai-move-check: checks the list sent from ai-index to see if any of those spaces are off already for efficiency.  (we don't want to toggle a light that is currently off)

## Printlo: prints a 3x3 game board that is displayed to the player.

## Toggle: Toggles the desired index either on or off depending on its current state. 

## arealloff:  Determines if all of the lights are all extinguished by returning True if so and Nil if not.  If it is true it signals the end of the game 