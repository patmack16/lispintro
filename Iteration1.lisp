(defun mycount (lst)
    (if (null  lst)
        0
        (+ 1 (mycount (cdr lst)))
    )
)


;;Eriks function 
(defun printlo(list)
    (if (= (mod (mycount list) 3 ) 0) ;;if count evenly divisible by 3
        (format t "~&~A" (car list)) ;;if divisible by 3 print the desired representation
        (format t "~A" (car list))
    )
    (if (null (cdr list))
        nil
        (printlo (cdr list))
    )
)
;;given an index, returns a list of indices that need to be toggled 
(defun toggle-logic(index)
    (cond 
        ((= index 0) '(0 1 3))
        ((= index 1) '(0 1 2 4))
        ((= index 2) '(1 2 5))
        ((= index 3) '(0 3 4 6))
        ((= index 4) '(1 3 4 5 7))
        ((= index 5) '(2 4 5 8))
        ((= index 6) '(3 6 7))
        ((= index 7) '(4 6 7 8))
        ((= index 8) '(5 7 8))
    )
)

;;toggles the desired index by counting down from the index to 0.  
(defun toggle (lst index)
    (if (= index 0)
        (cons (- 1(car lst))(cdr lst)) ;;if at 0 we are at the car of the list and it is toggled 
        (cons(car lst)(toggle(cdr lst) (- index 1)))));;if not at 0 yet continue thru the recursion 
        ;;returns a list  of indices to be toggled 


(defun toggle-logic(index)
    (cond 
        ((= index 0) '(0 1 3))
        ((= index 1) '(0 1 2 4))
        ((= index 2) '(1 2 5))
        ((= index 3) '(0 3 4 6))
        ((= index 4) '(1 3 4 5 7))
        ((= index 5) '(2 4 5 8))
        ((= index 6) '(3 6 7))
        ((= index 7) '(4 6 7 8))
        ((= index 8) '(5 7 8))
    )
)

;;recursively iterates thru the list returned from toggle-logic calls toggle() on the car 
(defun toggle-lst ( indices board)
    ;;(print indices)
    ;;(print board)
    (if (null indices)
        board
        (toggle-lst (cdr indices) (toggle board (car indices)))
    )

)

;;This function checks all of the indices returned from toggle-logic to see if they are off
;;If any of them are off already, another random number will be generated. 
(defun ai-move-check(indices lst)
    (if (null indices)
      t
      (if (= (nth (car indices) lst) 0)
          ;; first index is off
          (ai-move-check (cdr indices) lst)
           nil
      )
   )
)

;;Determines if all of the lights are off and the game is over 
(defun arealloff(lst)
(cond ;;if the list null or end of list return true
((null lst)t)

    ((= 1(car lst))nil) ;;if any light is on return false 

    (t(arealloff(cdr lst))) ;;keep recursing
    )
)

;;The new "AI function in the game"  This function generates a random index on the board.
;;  It then will call are off on the potential spots to be toggled.
;; IF any of the spots are off already recursion will continue and a new number generated

(defun ai-index (lst)
  (let ((idx (random 8)))
    (if (ai-move-check (toggle-logic idx) lst)
      (ai-index lst)
       idx
    )
  )
)




;;Replaeces one-move() from iteration 1.  Calls move counter, toggle-lst, and ai-index(the space picking function ).
;;essentially the same as before except calls a different function and adds a modified move counter. 
 (defun ai-move (lst move-count)
    (printlo lst)
    ;; print the count 
    (format T "~%This is move number ~d~%" move-count)
    (if (arealloff lst)
       
        (format T "Congratulations you Won!")
       
        (ai-move  ;;call one move on the list returned from toggle list
            (toggle-lst
                (toggle-logic (ai-index lst))
                lst
                
            )
            (+ move-count 1)
        )
    )
 )


;;Executes game 
(defun start-game ()
(ai-move '(0 1 0 1 0 0 1 0 1)0)

)
(start-game)